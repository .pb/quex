Quex
====
Quex is a operational management system for event production and hire companies.

This repository is a meta-repository for the Quex project.
The issue tracker in this repository tracks the requirements
identified for the project and the specifications for its functionality and implementation.